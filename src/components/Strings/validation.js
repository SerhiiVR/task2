const Validation = require('../validation');

/**
 * @exports
 * @class
 * @extends Validation
 */
class StringsValidation extends Validation {
    /**
     * @param {String} data.id - objectId
     * @returns
     * @memberof StringsValidation
     */
    findById(data) {
        return this.Joi
            .object({
                id: this.Joi.objectId(),
            })
            .validate(data);
    }

    /**
     * @param {String} profile.email
     * @param {String} profile.fullName
     * @returns
     * @memberof StringsValidation
     */
    create(profile) {
        // Changed
        return this.Joi
            .string()
            .min(1)
            .max(254)
            .required()
            .validate(profile);
    }

    /**
     * @param {String} data.id - objectId
     * @param {String} data.fullName
     * @returns
     * @memberof StringsValidation
     */
    updateById(data) {
        return this.Joi
            .object({
                id: this.Joi.objectId(),
                Strings: this.Joi
                    .string()
                    .min(1)
                    .max(30)
                    .required(),
            })
            .validate(data);
    }

    /**
     * @param {String} data.id - objectId
     * @returns
     * @memberof StringsValidation
     */
    deleteById(data) {
        return this.Joi
            .object({
                id: this.Joi.objectId(),
            })
            .validate(data);
    }
}

module.exports = new StringsValidation();
