const { Router } = require('express');
const Component = require('.');

/**
 * Express router to mount string related functions on.
 * @type {Express.Router}
 * @const
 */
const router = Router();

/**
 * Route serving list of string.
 * @name /v1/string
 * @function
 * @inner
 * @param {string} path - Express path
 * @param {callback} middleware - Express middleware.
 */
router.get('/', Component.findAll);

/**
 * Route serving a string
 * @name /v1/string/:id
 * @function
 * @inner
 * @param {string} path - Express path
 * @param {callback} middleware - Express middleware.
 */
router.get('/:id', Component.findById);

/**
 * Route serving a string
 * @name /v1/string/:id/LongestWord
 * @function
 * @inner
 * @param {string} path - Express path
 * @param {callback} middleware - Express middleware.
 */
router.get('/:id/LongestWord', Component.findLongestWord);

/**
 * Route serving a new string
 * @name /v1/string/:string/create
 * @function
 * @inner
 * @param {string} path - Express path
 * @param {callback} middleware - Express middleware
 */
router.get('/:string/create', Component.create);

/**
 * Route serving a new string
 * @name /v1/string
 * @function
 * @inner
 * @param {string} path - Express path
 * @param {callback} middleware - Express middleware
 */
router.put('/', Component.updateById);

/**
 * Route serving a new string
 * @name /v1/string
 * @function
 * @inner
 * @param {string} path -Express path
 * @param {callback} middleware - Express middleware
 */
router.delete('/', Component.deleteById);

module.exports = router;
