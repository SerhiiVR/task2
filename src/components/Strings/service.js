const Model = require('./model');

/**
 * @exports
 * @method findAll
 * @param {}
 * @summary get list of all string
 * @returns Promise<Model[]>
 */
function findAll() {
    return Model.find({}).exec();
}

/**
 * @exports
 * @method findById
 * @param {string} id
 * @summary get a string
 * @returns {Promise<Model>}
 */
function findById(id) {
    return Model.findById(id).exec();
}

/**
 * @exports
 * @method findById
 * @param {string} id
 * @summary get a object from DB
 * @returns {Promise<Model>}
 */
function findLongestWord(id) {
    return Model.findById(id).exec();
}

/**
 * @exports
 * @method create
 * @param {object} string
 * @summary create a new string
 * @returns {Promise<Model>}
 */
function create(string) {
    return Model.create({
        Strings: string,
    });
}

/**
 * Find a string by id and update his profile
 * @exports
 * @method updateById
 * @param {string} _id
 * @param {object} newProfile
 * @summary update a string's
 * @returns {Promise<void>}
 */
function updateById(_id, newProfile) {
    return Model.updateOne({ _id }, newProfile).exec();
}

/**
 * @exports
 * @method deleteById
 * @param {string} _id
 * @summary delete a string from database
 * @returns {Promise<void>}
 */
function deleteById(_id) {
    return Model.deleteOne({ _id }).exec();
}

module.exports = {
    findAll,
    findById,
    findLongestWord,
    create,
    updateById,
    deleteById,
};
