const { Schema } = require('mongoose');
const connections = require('../../config/connection');

const StringSchema = new Schema(
    {
        Strings: {
            type: String,
            trim: true,
        },
    },
    {
        collection: 'Strings',
        versionKey: false,
    },
);

module.exports = connections.model('Strings', StringSchema);
