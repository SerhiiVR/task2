const StringService = require('./service');
const StringValidation = require('./validation');
const ValidationError = require('../../error/ValidationError');

/**
 * @function
 * @param {express.Request} req
 * @param {express.Response} res
 * @param {express.NextFunction} next
 * @returns {Promise < void >}
 */
async function findAll(req, res, next) {
    try {
        const strings = await StringService.findAll();

        res.status(200).json({
            data: strings,
        });
    } catch (error) {
        res.status(500).json({
            error: error.message,
            details: null,
        });

        next(error);
    }
}

/**
 * @function
 * @param {express.Request} req
 * @param {express.Response} res
 * @param {express.NextFunction} next
 * @returns {Promise < void >}
 */
async function findById(req, res, next) {
    try {
        const { error } = StringValidation.findById(req.params);

        if (error) {
            throw new ValidationError(error.details);
        }

        const line = await StringService.findById(req.params.id);

        return res.status(200).json({
            data: line,
        });
    } catch (error) {
        if (error instanceof ValidationError) {
            return res.status(422).json({
                error: error.name,
                details: error.message,
            });
        }

        res.status(500).json({
            message: error.name,
            details: error.message,
        });

        return next(error);
    }
}


async function findLongestWord(req, res, next) {
    try {
        const { error } = StringValidation.findById(req.params);

        if (error) {
            throw new ValidationError(error.details);
        }

        const line = await StringService.findLongestWord(req.params.id);

        console.log(line.Strings);
        // Changed
        return res.status(200).json({
            Longest_Word: line.Strings.split(' ')
                .sort((a, b) => b.length - a.length)
                .shift(),
        });
    } catch (error) {
        if (error instanceof ValidationError) {
            return res.status(422).json({
                error: error.name,
                details: error.message,
            });
        }

        res.status(500).json({
            message: error.name,
            details: error.message,
        });

        return next(error);
    }
}

/**
 * @function
 * @param {express.Request} req
 * @param {express.Response} res
 * @param {express.NextFunction} next
 * @returns {Promise < void >}
 */
async function create(req, res, next) {
    try {
        const { error } = StringValidation.create(req.params.string);

        if (error) {
            throw new ValidationError(error.details);
        }
        console.log(req.params.string);
        const strings = await StringService.create(req.params.string);
        return res.status(200).json({
            data: strings,
        });
    } catch (error) {
        if (error instanceof ValidationError) {
            return res.status(422).json({
                message: error.name,
                details: error.message,
            });
        }

        res.status(500).json({
            message: error.name,
            details: error.message,
        });

        return next(error);
    }
}

/**
 * @function
 * @param {express.Request} req
 * @param {express.Response} res
 * @param {express.NextFunction} next
 * @returns {Promise<void>}
 */
async function updateById(req, res, next) {
    try {
        const { error } = StringValidation.updateById(req.body);

        if (error) {
            throw new ValidationError(error.details);
        }

        const updatedStrings = await StringService.updateById(req.body.id, req.body);

        return res.status(200).json({
            data: updatedStrings,
        });
    } catch (error) {
        if (error instanceof ValidationError) {
            return res.status(422).json({
                message: error.name,
                details: error.message,
            });
        }

        res.status(500).json({
            message: error.name,
            details: error.message,
        });

        return next(error);
    }
}

/**
 * @function
 * @param {express.Request} req
 * @param {express.Response} res
 * @param {express.NextFunction} next
 * @returns {Promise<void>}
 */
async function deleteById(req, res, next) {
    try {
        const { error } = StringValidation.deleteById(req.body);

        if (error) {
            throw new ValidationError(error.details);
        }

        const deletedStrings = await StringService.deleteById(req.body.id);

        return res.status(200).json({
            data: deletedStrings,
        });
    } catch (error) {
        if (error instanceof ValidationError) {
            return res.status(422).json({
                message: error.name,
                details: error.message,
            });
        }

        res.status(500).json({
            message: error.name,
            details: error.message,
        });

        return next(error);
    }
}

module.exports = {
    findAll,
    findById,
    findLongestWord,
    create,
    updateById,
    deleteById,
};
