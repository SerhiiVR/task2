const mongoose = require('mongoose');

const MONGODB_URI = 'mongodb+srv://admin:bPPFtYDRXbhKg0iM@Cluster0.l18j0.gcp.mongodb.net/Cluster0?retryWrites=true';
const MONGODB_DB_MAIN = 'cluster0';
const MONGO_URI = `${MONGODB_URI}${MONGODB_DB_MAIN}`;

const connectOptions = {
    // automatically try to reconnect when it loses connection
    autoReconnect: true,
    // reconnect every reconnectInterval milliseconds
    // for reconnectTries times
    reconnectTries: Number.MAX_VALUE,
    reconnectInterval: 1000,
    // flag to allow users to fall back to the old
    // parser if they find a bug in the new parse
    useNewUrlParser: true,
    useUnifiedTopology: true,
};

module.exports = mongoose.createConnection(MONGO_URI, connectOptions);
