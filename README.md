The program works with data base via REST requests.
Find the longest words in string from database by request.

Rune program: nodemon my-project/src/server/index.js

Add new string in data base:
http://localhost:3000/v1/string/<\some string>/create

Find by ID in data base:
http://localhost:3000/v1/string/<\some id> 5fac2321ab374a3bf0253fb1

Find longest word in string from data base:
http://localhost:3000/v1/string/<\id of string>/LongestWord

Dysplay all data from data base:
http://localhost:3000/v1/string/ - all strings and they id in DB, 
like [{"_id":"5fad25522bfd303d3c3d8037","Strings":"dsfsdf sdfsdfsdfsdf sfsdf dsfsdfsdf s"}]